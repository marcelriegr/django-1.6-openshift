import django
from django.shortcuts import render

def home(request):
    context = {'v': django.VERSION}
    return render(request, 'index.html', context)